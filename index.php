<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- TODO: add description and keywords to this page -->
	<meta name="description" content="Form "> 
	<meta name="keywords" content="Pulsar,Astropulse,Astronaut,UX,UI,isfates,dfhi">
	<meta name="author" content="+x">

	<title>Astropulse</title>

	<link rel="stylesheet" type="text/css" href="./library/Astronaut/astronaut.css">
	<link rel="stylesheet" type="text/css" href="./style/index.css">
	<link rel="icon" type="image/png" href="./img/favicon.png">

</head>
<body>


	<div id="warningRotate">
	    <!-- Free icon by https://icones8.fr/icons -->
		<img src="./img/rotate.png" alt="phone icon">
	</div>

	<!-- Backgound by Saransh Sinha -> https://codepen.io/saransh/pen/BKJun 	-->
	
	<div id="starsBackground">
		<div id='stars'></div>
		<div id='stars2'></div>
		<div id='stars3'></div>
	</div>


	<h1 id="welcomeSts">
		<span class="animMe">Welcome</span>
		<span class="animMe">to the</span>
		<span class="animMe text-gradient" id="mainWord">Astropulse</span>
		<span class="animMe">Experience</span>
	</h1>

	<div id="animationTools">
		<img src="./img/planet.png" id="planet" alt="red planet">
		<div id="innerPlanetExplosion"></div>
	</div>

	
	<a href="https://www.freepik.com/free-vector/mars-background-with-spacecraft_3285019.htm">Designed by Freepik</a>
	

	<div id="astronautCtn" class="astronaut-ctn">			
		<div id="astronaut">
			<div id="headsetBlur"></div>
			<img src="./img/astronaut.svg" alt="Astropulse character">
		</div>
	</div>

	<main id="main">
		<form action="./form.php" method="post" id="langCtn">
			<label class="animMe">
				<div id="en" onclick="selectLan(this, 'en')">
					<h2>English</h2>
					<img src="./img/flagEngland.svg" alt="english flag">
				</div>
				<input type="radio" name="lang" value="en" checked>
			</label>

			<label class="animMe">
				<div id="fr" onclick="selectLan(this, 'fr')">
					<h2>Français</h2>
					<img src="./img/flagFrance.svg" alt="french flag">
				</div>
				<input type="radio" name="lang" value="fr">
			</label>

			<label class="animMe">
				<div id="de" onclick="selectLan(this, 'de')">
					<h2>Deutsch</h2>
					<img src="./img/flagGermany.svg" alt="german flag">				
				</div>
				<input type="radio" name="lang" value="de">
			</label>		
			<input type="submit" name="submit" value="Select" id="button">
		</form>
	</main>
	
	

<script src="./library/jquery.js"></script>
<script src="./library/Astronaut/astronaut.js"></script>
<script>

var astronaut1 = new Astronaut("#astronautCtn", {
	"small" : 300,
	"large" : 350,
});
astronaut1.show({
	animation : "leftApparition 2s",
	marginTop : "150px",
});
astronaut1.speek(`<p>Arriveras-tu à garder ta notion du temps devant mes <span class="gold">icônes animées</span> ?<p style="font-size: 3em; margin-bottom: 0;">&#9757;</p></p>`, {
	animationDelay: "2s",
});

</script>
<script src="./script/index.js"></script>
</body>
</html>
