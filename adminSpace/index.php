<?php

require_once '../phpTools/dbConfig/dbObject.php';
require_once '../phpTools/dbConfig/astropulseDb.php';


AstropulseDB::connect();

$dbDatas = array(
	'responsesCount' => sizeof(AstropulseDB::get('SELECT * FROM user WHERE userFavoritePulsar IS NOT NULL')),

	'connexionsCount' => sizeof(AstropulseDB::get('SELECT * FROM user WHERE 1')),

	'frCount' => sizeof(AstropulseDB::get('SELECT * FROM user WHERE userFavoritePulsar IS NOT NULL AND lang="fr"')),

	'deCount' => sizeof(AstropulseDB::get('SELECT * FROM user WHERE userFavoritePulsar IS NOT NULL AND lang="de"')),

	'enCount' => sizeof(AstropulseDB::get('SELECT * FROM user WHERE userFavoritePulsar IS NOT NULL AND lang="en"')),

	'likeItCount' => sizeof(AstropulseDB::get('SELECT * FROM user_feedback WHERE likeIt="likeIt"')),
);

$dbDatas = json_encode($dbDatas);


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<title>Astropulse - AdminSpace</title>

	<link rel="stylesheet" type="text/css" href="../style/index.css">
	<link rel="icon" type="image/png" href="../img/favicon.png">

	<style type="text/css">
	
		table {
			font-size: 30px;
			border: none;
			border-collapse: collapse;
			margin-top: 50px;
			text-shadow: 2px 2px 20px #000;
			text-align: left;
		}
		table tr {
			border-bottom: solid #000 4px;
		}
		table tr:last-child {
			border-bottom: none;
			}
		td, th {			
			padding: 15px;
		}
		td {
			border-left: solid #000 4px;
		}


	</style>

</head>
<body>
	
	<div id="starsBackground">
		<div id='stars'></div>
		<div id='stars2'></div>
		<div id='stars3'></div>
	</div>


	<h1 id="welcomeSts" style="display: flex;">
		<span style="display: none;" class="animMe">Welcome</span>
		<span style="display: none;" class="animMe">to the</span>
		<span class="animMe text-gradient" id="mainWord">Astropulse</span>
		<span class="animMe">Experience</span>
	</h1>

	<main>

		<table></table>
	
	</main>

<script src="../library/jquery.js"></script>
<script>

	const header = {
		'responsesCount': 'Number of responses',
		'connexionsCount': 'Number of connexions',
		'frCount': 'Answers in French',
		'deCount': 'Answers in German',
		'enCount': 'Answers in English',
		'likeItCount': 'Like it counter',
	}
	const dbDatas = <?php echo ($dbDatas); ?>;
	for(let element in dbDatas) {
		let row = $('<tr></tr>');
		row.append($('<th></th>').text(header[element] + ' : '));
		row.append($('<td></td>').text(dbDatas[element]));
		$('table').append(row);		
	}

</script>
</body>
</html>
