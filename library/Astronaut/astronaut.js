
var deviceSize = "small";
if (screen.width >= 1200) {
    deviceSize = "large";
}

// ---- ASTRONAUT ----

var Astronaut = function(id, astronautWidthObj) {
    var id = id,
        _this = this,
        elmt = $(id),
        astronaut = $(id +">#astronaut" + "> img"),
        astronautWdt     = astronautWidthObj[deviceSize],
        astronautHgt     = null,
        astronautPstLeft = null,
        headsetBlur      = $(id +">#astronaut" + "> #headsetBlur"),
        headsetBlurWdt   = null,
        headsetBlurHgt   = null,
        headsetBlurTop   = null,
        headsetBlurLft   = null,
        headsetBlurBlur  = null,
        isSpeaking       = false,
        speechBox = $('<div id="speechBubble" class="speech-bubble"></div>');

    var init = function() {
        //astronautWdt    = astronaut.width();
        astronaut.css("width", astronautWdt);
        astronautHgt    = astronautWdt * 1.157;
        headsetBlurWdt  = astronautWdt * 0.4153;
        headsetBlurHgt  = astronautHgt * 0.1934;
        headsetBlurTop  = astronautHgt * 0.3315;
        headsetBlurLft  = astronautWdt * 0.38;
        headsetBlurBlur = astronautWdt * 0.064;
        
        headsetBlur.css({
            width  : headsetBlurWdt,
            height : headsetBlurHgt,
            top    : headsetBlurTop,
            left   : headsetBlurLft,
            filter : "blur(" + headsetBlurBlur + "px)",            
        });
    }

    this.show = function(obj=null) {
        init();

        if (obj != null) {
            elmt.css(obj);
        }

        astronautPstLeft = parseInt(elmt.css("marginLeft"));
        elmt.css("display", "inline-block");
    }

    this.hide = function(obj=null) {

        if (obj != null) {
            for (property in obj) {
                $(id).css(property,  obj[property]);
            }
        } else {
            if (isSpeaking) {
                this.hush();
            }
            $(id).fadeTo(1000, 0, function(){
                setTimeout(function() {
                    elmt.remove();
                }, 500);
            })
        }
    }

    this.resize = function(newWidth) {
        astronautWdt = newWidth;
        init();
    }

    this.speek = function(stc, obj=null) {
        if (!isSpeaking) {
            speechBox.html(stc);
            speechBox.css({
                position: "absolute",
                left: astronautPstLeft + parseInt(astronautWdt) + 50,
                marginTop: -astronautHgt + 50,
                animation: "fadingApparition 1s forwards",
            });
            if (obj != null) {
                speechBox.css(obj);
            }

            elmt.append(speechBox);
            isSpeaking = true;
        } else {
            let _this = this;
            this.hush();
            setTimeout(function() {
                _this.speek(stc);
            }, 1000)
        }
    }

    this.hush = function() {
        speechBox.css("animation", "fadingDisparition 1s forwards");
        setTimeout(function() {
            speechBox.remove();
        }, 1000);
        isSpeaking = false;
    }

}
// ---- END ASTRONAUT ----