// ---- PULSARFORM ----

var PulsarForm = function (id, param) {
	var userId = param.userId,
		id = id,
		introductoryText = param.introductoryText,
		hertzValue = param.hertzValue,
		pulsarWidth = param.pulsarWidth,
		formId = id + "Form",
		questionIndex = param.questionIndex,

		elmt = $(id),
		_this = this,
		title = $(`<h1 id="pulsarTitle">Test ${questionIndex}</h1>`),
		buttonStart = $("<div id=\"button-start\"></div>"),
		introductoryTextCtn = $(`<div id="introductingText"></div>`),
		buttonStop = $("<div id=\"button-stop\"></div>"),
		pulsarCtn = $("<div class=\"pulse-animation-ctn\"></div>"),
		pulsar = $("<div class=\"pulse-animation\"></div>"),
		validationAnimation = $("<div id=\"validationAnimation\"></div>"),
		timestampOnStart = null,
		userTime = null,
		isPulsing = false;
	formStructure = $('<form><div class="question"><h1>J’aime beaucoup cette icône</h1><div class="entry"><p>Pas du tout d\'accord</p><input type="radio" name="questionId0" value="0"><input type="radio" name="questionId0" value="1"><input type="radio" name="questionId0" value="2"><input type="radio" name="questionId0" value="3"><input type="radio" name="questionId0" value="4"><p>Tout à fait d\'accord</p></div></div><div class="question"><h1>Cette icône me donne le sentiment que le temps passe rapidement</h1><div class="entry"><p>Pas du tout d\'accord</p><input type="radio" name="lanquestionId1" value="0"><input type="radio" name="lanquestionId1" value="1"><input type="radio" name="lanquestionId1" value="2"><input type="radio" name="lanquestionId1" value="3"><input type="radio" name="lanquestionId1" value="4"><p>Tout à fait d\accord</p></div></div><input type="submit" name="submit" value="Select" id="button"></form>');

	var init = function () {
		introductoryTextCtn.append(introductoryText);
		elmt.addClass("pulsar-form");
	};

	init();

	this.showForm = function () {
		elmt.append(formStructure);
	}

	this.hiddeForm = function () {
		formStructure.remove();
	}

	this.startTest = function () {
		showButtonStart("start");

		makeElmtClickable(buttonStart);
		buttonStart.click(function () {
			timestampOnStart = new Date().getTime();
			hiddeButtonStart();
			_this.showPulsar();
			showButtonStop("Stop !");
			isPulsing = true;
		});
		stopPulseOnEvent();
	}

	this.getUserTime = function () {
		return userTime;
	}

	var showButtonStart = function (buttonText) {
		elmt.append(title);
		showPulsarCtn();
		buttonStart.html(buttonText);
		elmt.append(buttonStart);
		elmt.append(introductoryTextCtn);
	}

	var hiddeButtonStart = function () {
		introductoryTextCtn.remove();
		buttonStart.remove();
	}

	var showButtonStop = function (buttonText) {
		buttonStop.html(buttonText);
		elmt.append(buttonStop);
	}

	var hiddeButtonStop = function () {
		buttonStop.remove();
	};

	var showPulsarCtn = function () {
		let pulsarCtnWidth = parseInt(pulsarWidth.replace('px', '')) * 3 + 'px';
		pulsarCtn.css({
			width: pulsarCtnWidth,
			height: pulsarCtnWidth,
		});
		pulsar.css({
			width: pulsarWidth,
			height: pulsarWidth,
			animationDuration: hertzValue + "s",
		});
		$(id).append(pulsarCtn);
	}

	this.showPulsar = function () {
		pulsarCtn.append(pulsar);
	}

	this.hiddePulsar = function () {
		pulsarCtn.css({
			display: "none",
		});
	}

	var showValidationAnimation = function () {
		hiddeButtonStop();
		let formStructureInit = $("<form id=\"" + formId.replace("#", "") + "\" class=\"form\"></form>");
		elmt.append(formStructureInit);
		let form = new Form({
			id: formId,
			userId: userId,
			hertzValue: hertzValue,
			userTime: userTime,
			questions: param.formQuestions,
			satisfactionIndicator: param.formSatisfactionIndicator,
			sendDatasButtonText: param.formSendDatasButtonText,
		});
		// USER_TIMES is init in a script in form.php
		USER_TIMES[hertzValue] = userTime;
	}

	var getElapsedTime = function () {
		let timestampNow = new Date().getTime();
		let elapsedTime = timestampNow - timestampOnStart;
		return elapsedTime / 1000; // to get result in s not in ms
	}

	var stopPulseOnEvent = function () {
		$(window).keypress(function (e) {
			if (isPulsing && (e.key === ' ' || e.key === 'Spacebar')) {
				e.preventDefault()
				userTime = getElapsedTime();
				_this.hiddePulsar();
				isPulsing = false;
				showValidationAnimation();
			}
		});

		makeElmtClickable(buttonStop);
		buttonStop.click(function () {
			if (isPulsing) {
				userTime = getElapsedTime();
				_this.hiddePulsar();
				isPulsing = false;
				showValidationAnimation();
			}
		});
	}

}


var Form = function (param) {
	var id = param.id,
		questions = param.questions,
		satisfactionIndicator = param.satisfactionIndicator,
		sendDatasButtonText = param.sendDatasButtonText,
		hertzValue = param.hertzValue,
		userTime = param.userTime,
		userId = param.userId,

		elmt = $(id),
		goPrevButton = $(`<img id="goPrev" src="./img/arrow.svg">`),
		goNextButton = $(`<img id="goNext" src="./img/arrow.svg">`),
		validAnswersButton = $('<div id="validFormButton" class="validForm"></div>'),
		userAnswers = null;
	const INPUT_NUMBER = 5;

	var createForm = function () {
		elmt.append(goPrevButton);
		hiddeGoPrevButton();

		let questionsCtn = $("<div class=\"questions\"></div>");
		for (let questionIndex in questions) {
			let questionCtn = $("<div class=\"question\"></div>");
			let questionTitle = $("<h1></h1>").html(questions[questionIndex]);
			questionCtn.append(questionTitle);
			//progressBar.append($("<div></div>"));

			let questionEntry = $("<div class=\"entry\"></div>");
			questionEntry.append($("<p></p>").html(satisfactionIndicator.badIndicatorText));
			for (let inputIndex = 0; inputIndex < INPUT_NUMBER; inputIndex++) {
				let inputId = "inputId" + questionIndex + "_" + inputIndex;
				let radioLabel = $("<label for=\"" + inputId + "\" class=\"radio\"></label>");
				let input = $('<input>').attr({
					type: "radio",
					id: inputId,
					name: "questionId" + questionIndex,
					value: inputIndex,
				});
				questionEntry.append(input);
				questionEntry.append(radioLabel);
			}
			questionEntry.append($("<p></p>").html(satisfactionIndicator.goodIndicatorText));
			questionCtn.append(questionEntry);
			questionCtn.append(createProgressBar(questionIndex));
			questionsCtn.append(questionCtn);
			elmt.append(questionsCtn);
		}
		elmt.append(goNextButton);
	}

	var createProgressBar = function (n) {
		let progressBar = $('<div class="progressBar"></div>');
		for (let questionIndex in questions) {
			let bar = $("<div></div>");
			if (questionIndex <= n) {
				bar.addClass("inProgress");
			} else {
				bar.addClass("notInProgress");
			}
			progressBar.append(bar);
		}
		return progressBar;
	}

	var askQuestions = function () {
		let currentQuestion = 0;
		showQuestion(currentQuestion);

		makeElmtClickable($("#goNext"));
		$("#goNext").click(function () {
			if (document.querySelector("input[name=\"questionId" + currentQuestion + "\"]:checked") !== null) {
				if (currentQuestion < questions.length - 1) {
					currentQuestion += 1;
					showQuestion(currentQuestion);
					if (currentQuestion <= 1) {
						showGoPrevButton();
					}
					if (currentQuestion === questions.length - 1) {
						hiddeGoNextButton();

						makeElmtClickable($("label"));
						$("label").click(function () {
							showValidationButton()
						});
					}
				}
			} else {
				$(".question:nth-child(" + (parseInt(currentQuestion) + 1) + ")").css("animation", "error 0.8s ease");
				// Necessary to repeat animation
				setTimeout(function () {
					$(".question:nth-child(" + (parseInt(currentQuestion) + 1) + ")").css("animation", "none");
				}, 800);
			}
		});

		makeElmtClickable($("#goPrev"));
		$("#goPrev").click(function () {
			if (currentQuestion > 0) {
				currentQuestion -= 1;
				showQuestion(currentQuestion);
				if (currentQuestion === 0) {
					hiddeGoPrevButton();
				}
				if (currentQuestion <= questions.length - 1) {
					showGoNextButton();
				}
			}
		});
	}

	var hiddeGoPrevButton = function () {
		goPrevButton.css({
			opacity: "0",
			visibility: "hidden",
		});
	}
	var hiddeGoNextButton = function () {
		goNextButton.css({
			opacity: "0",
			visibility: "hidden",
		});
	}
	var showGoPrevButton = function () {
		goPrevButton.css({
			opacity: "1",
			visibility: "visible",
		});
	}
	var showGoNextButton = function () {
		goNextButton.css({
			opacity: "1",
			visibility: "visible",
		});
	}


	var showQuestion = function (questionIndex) {
		hiddeAllQuestions();
		$(".question:nth-child(" + (parseInt(questionIndex) + 1) + ")").css({
			display: "flex",
			animation: "fadingApparition 0.5s",
		});
	}

	var hiddeAllQuestions = function () {
		for (let questionIndex in questions) {
			$(".question:nth-child(" + (parseInt(questionIndex) + 1) + ")").css({
				display: "none",
				animation: "none",
			});
		}
	}

	var getCheckedRadioValue = function (radioName) {
		return document.querySelector("input[name=\"" + radioName + "\"]:checked").value;
	}

	var showValidationButton = function () {
		validAnswersButton.text(sendDatasButtonText);
		elmt.parent().append(validAnswersButton);

		makeElmtClickable(validAnswersButton);
		validAnswersButton.click(function () {
			getUserAnswers();
			showValidationPage();
		});
	}

	var showValidationPage = function () {
		$("#pulsarTitle").remove();
		elmt.parent().append('<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg id="validationAnimation" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 108 108"><style>#group { transform: rotate(90deg); animation: rotate 800ms 100ms ease-in forwards, scale 200ms 1400ms ease-in forwards; transform-origin: center center; } #circle-3 { animation: line 800ms 100ms ease-in forwards; stroke-dasharray: 500; stroke-dashoffset: 500; opacity: 0.6; } #circle-2 { animation: line 800ms 250ms ease-in forwards; stroke-dasharray: 500; stroke-dashoffset: 500; opacity: 0.6; } #circle { stroke-dasharray: 500; stroke-dashoffset: 500; animation: line 800ms 400ms ease-in forwards; opacity: 0.6; } #valid { animation: line 500ms 1000ms ease-in forwards; stroke-dasharray: 100; stroke-dashoffset: 100; } @keyframes line { to { stroke-dashoffset: 0; } } @keyframes scale { 50% { transform: scale(0.8) } to { transform: scale(1) } }</style><g id="group" stroke="#2ad193" stroke-width="6" fill="none"><path id="circle-3" d="M54 103c27.062 0 49-21.938 49-49S81.062 5 54 5 5 26.938 5 54s21.938 49 49 49z"/><path id="circle-2" d="M54 103c27.062 0 49-21.938 49-49S81.062 5 54 5 5 26.938 5 54s21.938 49 49 49z"/><path id="circle" d="M54 103c27.062 0 49-21.938 49-49S81.062 5 54 5 5 26.938 5 54s21.938 49 49 49z"/><path id="valid" d="M29 53.123l17.455 17.456L79 38"/></g></svg>');
		elmt.remove();
		validAnswersButton.remove();
		$(document).trigger("hasAnswerToForm");
	}

	var getUserAnswers = function () {
		let answers = {
			userId: userId,
			hertzValue: hertzValue,
			userTime: userTime,
		};
		for (let inputIndex in questions) {
			answers["questionId" + inputIndex] = parseFloat(getCheckedRadioValue("questionId" + inputIndex));
		}
		userAnswers = answers;

		sendDatas(answers);
	}

	var sendDatas = function (object) {
		$.ajax({
			url: './phpTools/insertUserAnswers.php',
			type: 'POST',
			data: object,
		});
	}

	var init = function () {
		createForm();
		askQuestions();
	}
	init();
}



// Force element to trigger click event on click
function makeElmtClickable(elmt) {
	elmt.attr('onclick', '');
}