var choosenLan = "en";

// Wait until window is load
window.onload = function() {

	planetExplosionAnmt();

	selectLan(document.getElementById("en"));

};

	
function selectLan(e, lan="en") {
	unselectAll();
	e.style.opacity = "1";
	e.style.borderLeftColor = "#fff";
	// Anim check
	e.children[1].style.animation = "bounce 0.5s";
	choosenLan = lan;
}

function unselectAll() {
	for (let e of [document.getElementById("en"), document.getElementById("fr"), document.getElementById("de")]) {
		e.style.opacity = "0.5"
		e.style.borderLeftColor = "rgba(0,0,0,0)";
		e.children[1].style.animation = "";
	}
}

// ---- ANIMATION ----
var planet = $("#planet");
var innerPlanetExpl = $("#innerPlanetExplosion");
var welcomeSts = $("#welcomeSts");
function planetExplosionAnmt() {
	planet.fadeTo(2000, 1);
	setTimeout( brokenPlanetAnmt, 2500 );
}

function brokenPlanetAnmt() {
	innerPlanetExpl.css({
		top: planet.position().top + (planet.height() / 2),
		left:  planet.position().left + (planet.width() / 2)
	});
	innerPlanetExpl.css("display", "inline");
	planet.attr("src", "./img/planetBroken.png")
	planet.css("animation", "rotate 40s infinite linear");
	setTimeout( function(){
		innerPlanetExpl.css("display", "none").remove();
		planet.css("background", "radial-gradient(circle, rgba(221,46,41,1), rgba(221,46,41,0.3), rgba(221,46,41,0)50%)");
		animMyChildren("#welcomeSts", 0.2);
		welcomeSts.css("display", "flex");
		setTimeout(function() {
			document.getElementById("mainWord").style.animation = "textShake 3s infinite alternate";
			document.getElementById("mainWord").style.opacity = "1";
			welcomeSts.css("margin-top", "2vh");

			// Create the astronaut
			var astronaut1 = new Astronaut("#astronautCtn", {
				"small" : 300,
				"large" : 350,
			});
			astronaut1.show({
				animation : "leftApparition 2s",
				marginTop : "150px",
			});
			astronaut1.speek("<p>Please choose your language before we start the adventure !</p>", {
				animationDelay: "2s",
			});
			
			setTimeout(function() {
				astronaut1.hide();
				animMyChildren("#langCtn", 0.4, 1.5, 2);
				$("#langCtn").css("display", "flex");

				setTimeout(function() {
					$("#button").css("display", "flex").fadeTo("slow", 1);
				}, 4000);
			}, 6000);
		}, 2500);
	}, 1000 );
}

// Delay is in seconde (s)
function animMyChildren(ctnId, delay, animDuration=1, firstDelay=0) {
	let animDelay = firstDelay;
	let elmtToAnim = $(ctnId + " > .animMe");
	for (let elmtIndex = 0; elmtIndex < elmtToAnim.length; elmtIndex++) {
		elmtToAnim[elmtIndex].style.opacity = "0";
	    elmtToAnim[elmtIndex].style.animationName = 'rotation';
	    elmtToAnim[elmtIndex].style.animationDelay = animDelay + "s";
	    elmtToAnim[elmtIndex].style.animationDuration = animDuration + 's';
	    elmtToAnim[elmtIndex].style.WebkitAnimationFillMode = 'forwards';
		
		animDelay += delay;
	}
}

// ---- END OF ANIMATION ----