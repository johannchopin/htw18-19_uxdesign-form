
var sentences = allSentences[LANG];

// ***** SLIDESHOW LIBRARY INITIALISATION *****

// Create Swiper object
var mySwiper = new Swiper('.swiper-container', {
    // Set direction from the slider
    direction: "vertical",

    // InitialSlide: 0,
    preloadImages: true,

    // Create Next-Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
    },

    // User don't have to go back in the form
    allowSlidePrev: false,

    allowTouchMove: true, // TODO: Set allowTouchMove to false

    // User can't swipe -> he have to use the "next button" who have to be unlocked
    noSwiping: false, // TODO: Set parameter to true


    //initialSlide: 6, // TODO: Remove initialSlide parameter
});

// ***** INITIALISATION END *****

const DEVICE_SIZE = "small";
if (screen.width >= 1200) {
    const DEVICE_SIZE = "large";
}


const BUTTON_NEXT = document.getElementById("buttonNext");
var screenHgt = $(document).height();
var hertzValues = [0.5, 1, 2];

// How many % from the screen does bg go up on slide change
var goUpDistanceInPrct = 0.3;


$(window).on('load', function () {

    makeElmtClickable($(".goNextQuestionButton"));
    $(".goNextQuestionButton").click(function () {
        mySwiper.slideNext(1000);
    });

    $(document).on("hasAnswerToForm", function () {
        setTimeout(function () {
            mySwiper.slideNext(1000);
        }, 2000);
    });

    setTimeout(function () {
        $("#loader").remove();
        $("#bg").height(mySwiper.height * $(".swiper-slide").length);
        checkSlideIndex();

        mySwiper.on('slideChange', function () {
            moveBackground($("#bg"));
            checkSlideIndex();
        });
    }, 0);

});


function checkSlideIndex() {
    switch (mySwiper.activeIndex) {
        case 0:
            initSlider0();
            break;
        case 1:
            initSlider1();
            break;
        case 2:
            initSlider2();
            break;
        case 3:
            initSlider3();
            break;
        case 4:
            initSlider4();
            break;
        case 5:
            initSlider5();
            break;
        case 6:
            initSlider6();
            break;
    }
}

function initSlider0() {
    let goNextQuestionButton = $(".swiper-slide:nth-child(1) > .goNextQuestionButton");
    initGoNextQuestionButton(goNextQuestionButton, sentences.swipe.startButtonText);
    var astronaut1 = new Astronaut("#astronautCtn1", {
        small: 250,
        large: 300,
    });
    astronaut1.show({
        animation: "leftApparition 2s forwards",
        marginLeft: "-10px",
    });
    astronaut1.speek(sentences.astronaut.presentationText, {
        animationDelay: "2s",
        width: "60vw",
        marginLeft: "-50px",
    });
    setTimeout(function () {
        goNextQuestionButton.css("display", "inline-block");
    }, 7000);
}

function initSlider1() {
    let goNextQuestionButton = $(".swiper-slide:nth-child(2) > .goNextQuestionButton");
    initGoNextQuestionButton(goNextQuestionButton, sentences.swipe.goNextButtonText);
    let pulsarFrequenz = getRandom();
    var pulsar1 = new PulsarForm("#pulsar1", {
        userId: USER_ID,
        hertzValue: pulsarFrequenz,
        pulsarWidth: "70px",
        questionIndex: 1,
        introductoryText: sentences.pulsar.introductoryText,
        formSatisfactionIndicator: sentences.pulsar.form.satisfactionIndicator,
        formQuestions: sentences.pulsar.form.questions,
        formSendDatasButtonText: sentences.pulsar.form.sendDatasButtonText,
    });
    pulsar1.startTest();
}

function initSlider2() {
    let goNextQuestionButton = $(".swiper-slide:nth-child(3) > .goNextQuestionButton");
    initGoNextQuestionButton(goNextQuestionButton, sentences.swipe.goNextButtonText);
    let pulsarFrequenz = getRandom();
    var pulsar2 = new PulsarForm("#pulsar2", {
        userId: USER_ID,
        hertzValue: pulsarFrequenz,
        pulsarWidth: "70px",
        questionIndex: 2,
        introductoryText: sentences.pulsar.introductoryText,
        formSatisfactionIndicator: sentences.pulsar.form.satisfactionIndicator,
        formQuestions: sentences.pulsar.form.questions,
        formSendDatasButtonText: sentences.pulsar.form.sendDatasButtonText,
    });
    pulsar2.startTest();
}

function initSlider3() {
    let goNextQuestionButton = $(".swiper-slide:nth-child(4) > .goNextQuestionButton");
    initGoNextQuestionButton(goNextQuestionButton, sentences.swipe.goNextButtonText);
    let pulsarFrequenz = getRandom();
    var pulsar3 = new PulsarForm("#pulsar3", {
        userId: USER_ID,
        hertzValue: pulsarFrequenz,
        pulsarWidth: "70px",
        questionIndex: 3,
        introductoryText: sentences.pulsar.introductoryText,
        formSatisfactionIndicator: sentences.pulsar.form.satisfactionIndicator,
        formQuestions: sentences.pulsar.form.questions,
        formSendDatasButtonText: sentences.pulsar.form.sendDatasButtonText,
    });
    pulsar3.startTest();
}

function initSlider4() {
    let showUserTimesButton = $(".swiper-slide:nth-child(5) > #showUserTimesButton")
    let goNextQuestionButton = $(".swiper-slide:nth-child(5) > .goNextQuestionButton");
    initGoNextQuestionButton(goNextQuestionButton, sentences.swipe.goNextButtonText);
    showUserTimesButton.html(sentences.swipe.showUserTimesText);
    let title = $("#WDYPQuestion");
    title.append(sentences.swipe.whichPulsarDoYouPreferQuestion);

    makeElmtClickable($("#pulsesAnimationCtn .pulse-animation-choice"));
    $("#pulsesAnimationCtn .pulse-animation-choice").click(function () {
        showUserTimesButton.css("display", "inline-block");

        makeElmtClickable(showUserTimesButton);
        showUserTimesButton.click(function () {
            $(".swiper-slide:nth-child(5) label .radio").css("display", "none");
            $(".swiper-slide:nth-child(5) #userTimesCtn p:nth-child(1)").text(`${USER_TIMES["0.5"]}s`);
            $(".swiper-slide:nth-child(5) #userTimesCtn p:nth-child(2)").text(`${USER_TIMES["1"]}s`);
            $(".swiper-slide:nth-child(5) #userTimesCtn p:nth-child(3)").text(`${USER_TIMES["2"]}s`);
            $(".swiper-slide:nth-child(5) #userTimesCtn").css("display", "flex");
            showUserTimesButton.css("display", "none");

            setTimeout(function () {
                goNextQuestionButton.css("display", "inline-block");
            }, 2000);

            let userFavoritePulsar = {
                userId: USER_ID,
                userFavoritePulsar: parseFloat($('input[name=favoritePulsar]:checked').val()),
            }
            sendObjectToPhpScript(userFavoritePulsar, "./phpTools/insertUserFavoritePulsar.php");
        });
    });
}

function initSlider5() {

    // Remove it because of google chrome mobile keyboard bug
    $("#warningRotate").css("display", "none");

    let feedbackFormSubmit = $("#feedbackFormSubmit");
    $("#feedbackForm #likeItQuestion").append(sentences.swipe.feedbackForm.likeItText);
    $("#feedbackForm label[for='email']").append(sentences.swipe.feedbackForm.emailText);
    $("#feedbackForm label[for='message']").append(sentences.swipe.feedbackForm.messageText);
    $("#feedbackForm #feedbackFormSubmit").append(sentences.swipe.feedbackForm.sendButtonText);
    $("#feedbackForm #escapeForm").append(sentences.swipe.feedbackForm.goNextButtonText);
    var astronaut2 = new Astronaut("#astronautCtn2", {
        small: 250,
        large: 250,
    });
    astronaut2.show({
        animation: "leftApparition 2s forwards",
        marginLeft: "-10px",
        marginTop: "-40px",
    });
    astronaut2.speek(sentences.astronaut.endingText.thankYouSentence, {
        animationDelay: "2s",
        width: "60vw",
    });
    setTimeout(function () {
        astronaut2.speek(sentences.astronaut.endingText.giveEmail, {
            animationDelay: "2s",
            width: "60vw",
        });
        setTimeout(function () {
            $("#feedbackForm").css("display", "flex");
            makeElmtClickable($("#feedbackForm #feedbackFormSubmit"));
            $("#feedbackForm #feedbackFormSubmit").click(function () {
                let userFeedback = {
                    userId: USER_ID,
                    likeIt: $('input[name=likeIt]:checked', '#feedbackForm').val(),
                    userMail: $('input[type=email]', '#feedbackForm').val(),
                    userMessage: $('#feedbackForm #message').val(),
                }
                if (validateEmail(userFeedback.userMail)) {
                    $("#feedbackForm #feedbackFormSubmit").remove();
                    sendObjectToPhpScript(userFeedback, "./phpTools/insertUserFeedback.php");
                    mySwiper.slideNext(1000);
                } else {
                    $("#feedbackForm input[type=email]").css("border-color", "red");
                }
            });
            makeElmtClickable($("#feedbackForm #escapeForm"));
            $("#feedbackForm #escapeForm").click(function () {
                mySwiper.slideNext(1000);
            });
        }, 4000);
    }, 8000);
}

function initSlider6() {
    let slider6 = $(".swiper-slide:nth-child(7)");
    let slider6Content = `
        <div id="astronautCtn3" class="astronaut-ctn">          
            <div id="astronaut">
                <div id="headsetBlur"></div>
                <img src="./img/astronaut.svg" alt="Astropulse character">
            </div>
        </div>
        <div class="social-ctn" id="socialCtn">
            <a class="social-button animMe" id="facebookButton" target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=https://astropulse.johannchopin.fr/">
                <img src="./img/facebookIcon.svg" alt="facebook icon">
            </a>
            <a class="social-button animMe" id="twitterButton" target="_blank" href="https://twitter.com/share?url=https://astropulse.johannchopin.fr/&text=${sentences.swipe.contacts.twitterShareBoxText}">
                <img src="./img/twitterIcon.svg" alt="twitter icon">
            </a>
            <a href="mailto:contact-astropulse@protonmail.com" class="animMe" id="mailButton">
                <img src="./img/mailIcon.svg" alt="mail icon">
            </a>
            <p class="plainMail animMe">contact-astropulse@protonmail.com</p>
        </div>`;

    slider6.html(slider6Content);

    let facebookButton = $("#facebookButton"),
        twitterButton = $("#twitterButton"),
        mailButton = $("#mailButton"),
        allButtons = [facebookButton, twitterButton, mailButton];
    var astronaut3 = new Astronaut("#astronautCtn3", {
        small: 250,
        large: 250,
    });
    astronaut3.show({
        animation: "leftApparition 2s forwards",
        marginLeft: "-10px",
        marginTop: "-40px",
    });
    astronaut3.speek(sentences.astronaut.endingText.shareMeSentence, {
        animationDelay: "2s",
        width: "60vw",
    });

    facebookButton.append(`<p>${sentences.swipe.contacts.facebookText}</p>`);
    twitterButton.append(`<p>${sentences.swipe.contacts.twitterText}</p>`);
    mailButton.append(`<p>${sentences.swipe.contacts.mailText}</p>`);

    // Problem with js asynchronous
    // Wait 0.2s that slider6Content is loaded
    setTimeout(function () {

        // Necessary to fix the keyboard bug in mobile browsers
        if (DEVICE_SIZE === "small") {
            slider6.css({
                height: "+=" + Math.abs($('#astronautCtn3').offset().top),
            });
            setTimeout(function () {
                $("#astronautCtn3").css("margin-top", Math.abs($('#astronautCtn3').offset().top));
            }, 1000);
        }

        let buttonsWidth = [];
        let buttonsHeight = [];
        for (let button of allButtons) {
            buttonsWidth.push(parseInt(button.css("width")));
            buttonsHeight.push(parseInt(button.css("height")));
        }

        let largestButtonWidth = Math.max.apply(Math, buttonsWidth) + 4;
        let highestButtonheight = Math.max.apply(Math, buttonsHeight) + 4;
        for (let button of allButtons) {
            button.css({
                width: largestButtonWidth,
                height: highestButtonheight,
            });
        }
    }, 200);

    animMyChildren("#socialCtn", 0.2, 1, 3);

    makeElmtClickable(facebookButton);
    facebookButton.click(function () {
        return !window.open(this.href, 'Facebook', 'width=640,height=300');
    });
    makeElmtClickable(twitterButton);
    twitterButton.click(function () {
        return !window.open(this.href, 'Twitter', 'width=640,height=300')
    });
    makeElmtClickable($("#socialCtn .plainMail"));
    $("#socialCtn .plainMail").click(function () {
        copyToClipboard("contact-astropulse@protonmail.com");
    });
}


function animMyChildren(ctnId, delay, animDuration = 1, firstDelay = 0) {
    let animDelay = firstDelay;
    let elmtToAnim = $(ctnId + " .animMe");
    for (let elmtIndex = 0; elmtIndex < elmtToAnim.length; elmtIndex++) {
        elmtToAnim[elmtIndex].style.opacity = "0";
        elmtToAnim[elmtIndex].style.animationName = 'rotation';
        elmtToAnim[elmtIndex].style.animationDelay = animDelay + "s";
        elmtToAnim[elmtIndex].style.animationDuration = animDuration + 's';
        elmtToAnim[elmtIndex].style.WebkitAnimationFillMode = 'forwards';

        animDelay += delay;
    }
}

function validateEmail(email) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

function initGoNextQuestionButton(elmt, text) {
    elmt.html(text);
}

function sendObjectToPhpScript(object, phpScriptDirectory) {
    $.ajax({
        url: phpScriptDirectory,
        type: 'POST',
        data: object,
    });
}

function getRandom() {
    let value = hertzValues[Math.floor(Math.random() * hertzValues.length)];
    for (let i = 0; i < hertzValues.length; i++) {
        if (hertzValues[i] === value) {
            hertzValues.splice(i, 1);
        }
    }
    return value;
}

// Force element to trigger click event on click
function makeElmtClickable(elmt) {
    elmt.attr('onclick', '');
}

function moveBackground(e) {
    let goUpDistance = screenHgt * goUpDistanceInPrct;
    e.css("top", "-=" + goUpDistance);
}


function copyToClipboard(text) {
    window.prompt("Copy to clipboard: ", text);
}