var allSentences = {
	en: {
		astronaut: {
			presentationText: `
		        <p>Hello World !</p>
		        <p>We will present you one after the other <span class="green">3 animated icons</span>.</p>
		        <p>Your task is to stop each icons after exactly <span class="red">10 seconds</span> by pressing the spacebar/clicking the <span class="stop-button">STOP</span> button</p>
		        <p>This study takes no longer than 2 minutes.</p>
		        <p>Thank you in advance for your participation! <span class="red">&#10084;</span></p>`,
			endingText: {
				thankYouSentence: `
					<p>Thank you very much, very friendly user <span class="gold">&#10084;</span></p> 
					<p>This study will allow me to create icons that are more pleasant to look at when it comes to loading times on the Internet!</p>`,
				giveEmail: `<p>Give me your e-mail address and your impressions so that I can send you the results of the survey and improve myself.</p>`,
				shareMeSentence: `
					<p class="gold">Thank you very much for your help :)</p>
					<p>If you want to help me even more, <span class="red">share as much as possible</span> with your friends' users! It would make me very happy <span class="green">^_^</span></p>
					<p style="font-size: 3em; margin-bottom: 0;">&#9757;</p>`,
			},
		},
		pulsar: {
			introductoryText: `<p>Are you ready for this?</p><p>Your task is to stop each icon after exactly <span class="red">10 seconds</span> by pressing the spacebar/clicking the <span class="stop-button">STOP</span> button</p>`,
			form: {
				questions: [
					"I really like this icon",
					"This icon gives me the feeling that time flies fast",
					"This icon disturbs me",
					"The blinking of this icon forces me to stare at it",
				],
				satisfactionIndicator: {
					goodIndicatorText: "Agree",
					badIndicatorText: "Disagree",
				},
				sendDatasButtonText: "Send your answers",
			},
		},
		swipe: {
			startButtonText: "Start",
			goNextButtonText: "Continue",
			whichPulsarDoYouPreferQuestion: "Which of the 3 icons you tested do you prefer?",
			showUserTimesText: "Show the results of my test",
			feedbackForm: {
				likeItText: "Did you like it?",
				emailText: "Your email address",
				messageText: "your message",
				sendButtonText: "Send",
				goNextButtonText: "No, I'm fine",
			},
			contacts: {
				facebookText: "Share me on Facebook",
				twitterText: "Share me on Twitter",
				twitterShareBoxText: `Will you be able to keep your sense of time in front of my animated icons? I look forward to your results :) =>`,
				mailText: "Contact me !",
			},
		},
	},



	de: {
		astronaut: {
			presentationText: `
		        <p>Hello World !</p>
		        <p>Wir werden euch nacheinander <span class="green">3 animierte Icons</span> präsentieren.</p>
		        <p>Ihre Aufgabe ist es, jedes Symbol nach exact <span class="red">10 Sekunden</span> zu stoppen, indem Ihr entweder auf die Leertaste drückt oder die <span class="stop-button">STOP</span> Taste klickt</p>
		        <p>Diese Studie dauert nicht länger als 2 Minuten.</p>
		        <p>Vielen Dank im Voraus für Eure Teilnahme! <span class="red">&#10084;</span></p>`,
			endingText: {
				thankYouSentence: `
					<p>Vielen Dank, netter Nutzer <span class="gold">&#10084;</span></p> 
					<p>Diese Studie wird mir ermöglichen, Icons zu erstellen, die angenehmer zu betrachten sind, wenn es um die Ladezeiten im Internet geht!</p>`,
				giveEmail: `<p>Gib mir deine E-Mail-Adresse und deine Eindrücke, damit ich dir die Ergebnisse der Umfrage schicken und mich verbessern kann.</p>`,
				shareMeSentence: `
					<p class="gold">Vielen Dank für deine Hilfe :)</p>
					<p>Wenn du mir noch mehr helfen willst, <span class="red">teile so viel wie möglich</span> mit deine Nutzerfreunden! Es würde mich sehr freuen <span class="green">^_^</span></p>
					<p style="font-size: 3em; margin-bottom: 0;">&#9757;</p>`,
			},
		},
		pulsar: {
			introductoryText: `<p>Bist du bereit?</p><p>Ihre Aufgabe ist es, jedes Symbol nach exact <span class="red">10 Sekunden</span> zu stoppen, indem Sie die Leertaste drücken/auf die <span class="stop-button">STOP</span> Taste klicken</p>`,
			form: {
				questions: [
					"Ich mag dieses Symbol wirklich",
					"Dieses Symbol gibt mir das Gefühl, dass die Zeit schnell vergeht",
					"Dieses Symbol stört mich",
					"Das Blinken dieses Symbols zwingt mich, es anzustarren",
				],
				satisfactionIndicator: {
					goodIndicatorText: "Einverstanden",
					badIndicatorText: "Nicht einverstanden",
				},
				sendDatasButtonText: "Deine Antworten senden",
			},
		},
		swipe: {
			startButtonText: "Beginnen",
			goNextButtonText: "Weiter",
			whichPulsarDoYouPreferQuestion: "Welches der 3 Symbole, die du getestet hast, bevorzugst du?",
			showUserTimesText: "Die Ergebnisse meines Tests anzeigen",
			feedbackForm: {
				likeItText: "Hat es dir gefallen?",
				emailText: "Deine E-Mail-Adresse",
				messageText: "Ihre Nachricht",
				sendButtonText: "Senden",
				goNextButtonText: "Nein, danke",
			},
			contacts: {
				facebookText: "Teile mich auf Facebook",
				twitterText: "Teile mich auf Twitter",
				twitterShareBoxText: `Wirst du in der Lage sein, dein Zeitgefühl vor meinen animierten Symbolen zu bewahren? Ich freue mich auf Ihre Ergebnisse :) =>`,
				mailText: "Kontaktiere mich !",
			},
		},
	},



	fr: {
		astronaut: {
			presentationText: `
		        <p>Hello World !</p>
		        <p>Nous allons vous présenter <span class="green">3 icônes</span> animées les unes après les autres.</p>
		        <p>Votre tâche consiste à arrêter chaque icône après exactement <span class="red">10 secondes</span>, en appuyant sur la barre d’espacement/en cliquant sur le bouton <span class="stop-button">STOP</span></p>
		        <p>Cette étude ne vous prendra pas plus de 2 minutes.</p>
		        <p>Merci d’avance pour votre participation ! <span class="red">&#10084;</span></p>`,
			endingText: {
				thankYouSentence: `
					<p>Merci beaucoup gentil utilisateur <span class="gold">&#10084;</span></p> 
					<p>Cette étude va me permettre de créer des icônes plus agréable à regarder pour les temps de chargement sur Internet !</p>`,
				giveEmail: `<p>Donne moi ton adresse mail ainsi que tes impressions pour que je puisse te transmettre les résultats de l'enquête et m'améliorer</p>`,
				shareMeSentence: `
					<p class="gold">Merci beaucoup pour ton aide :)</p>
					<p>Si tu as envie de m'aider encore plus <span class="red">partage</span> un maximum auprès de tes amis utilisateurs ! Ça me ferait super plaisir <span class="green">^_^</span></p>
					<p style="font-size: 3em; margin-bottom: 0;">&#9757;</p>`,
			},
		},
		pulsar: {
			introductoryText: `<p>Etes-vous prêt(e) ?</p><p>Vous allez voir une icône animée que vous devez arrêter après exactement <span class="red">10 secondes</span>, en appuyant sur la barre d’espacement/en cliquant sur le bouton <span class="stop-button">STOP</span></p>`,
			form: {
				questions: [
					"J’aime beaucoup cette icône",
					"Cette icône me donne le sentiment que le temps passe rapidement",
					"Cette icône m’ennuie",
					"Le clignotement de cette icône m’oblige à la regarder fixement",
				],
				satisfactionIndicator: {
					goodIndicatorText: "Tout à fait d’accord",
					badIndicatorText: "Pas du tout d’accord",
				},
				sendDatasButtonText: "Envoyer vos réponses",
			},
		},
		swipe: {
			startButtonText: "Commencer",
			goNextButtonText: "Suivant",
			whichPulsarDoYouPreferQuestion: "Parmi les 3 icônes que vous avez testées, laquelle préférez-vous ?",
			showUserTimesText: "Montrer les résultats de mon test",
			feedbackForm: {
				likeItText: "Avez-vous apprécié ?",
				emailText: "votre adresse mail",
				messageText: "votre message",
				sendButtonText: "Envoyer",
				goNextButtonText: "Non merci",
			},
			contacts: {
				facebookText: "Partage moi sur Facebook",
				twitterText: "Partage moi sur Twitter",
				twitterShareBoxText: `Arriveras-tu as garder ta notion du temps devant mes icônes animées ? J'attends avec hâte tes résultats :) =>`,
				mailText: "Contact moi !",
			},
		},
	},
}