<?php

class Database
{
    private static $bdd;

    public static function connect_($dbName, $username, $password) 
    {
        self::$bdd = new PDO('mysql:host=localhost;dbname=' . $dbName . ';charset=utf8', $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT => true));
    }

    public static function disconnect() 
    {
        self::$bdd = null;
    }

    public static function query($sql, $values) 
    {
        $req = self::$bdd->prepare($sql);
        
        // TODO -> Add type test like :
        // bindParam(':'.$valueKey, $value, PDO::PARAM_STR);
        foreach ($values as $valueKey => &$value) {
            $req->bindValue(':'.$valueKey, $value);
        }

        $req->execute();
    }

    public static function get($sql) 
    {
        $statement = self::$bdd -> prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

};

    
?>
