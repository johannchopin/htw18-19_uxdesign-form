<?php

function getUserInfos() {
    $timestamp = date_timestamp_get(date_create());
    $userIp = getUserIp();
    $userDevice = getUserDevice();

    $user = array(
        "userId"    => crypt(urlencode($timestamp . $userIp), rand(11, 1000)) . crypt($timestamp, rand(11, 1000)),
        "timestamp" => $timestamp,
        "device"    => $userDevice,
     );

    return $user;
}

function getUserIp() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_User_IP']))
        $ipaddress = $_SERVER['HTTP_User_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function getUserDevice() {
	if (isMobile()) {
		return "mobile";
	}
	return "computer";
}
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

?>