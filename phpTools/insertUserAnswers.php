<?php  

$headers = apache_request_headers();

if ( $headers['Referer'] == 'http://localhost:8082/htw18-19_uxdesign-formGITLAB/form.php'  // TODO: Change Referer URL
	&& isset($_POST["userId"])
	&& isset($_POST["hertzValue"])
	&& isset($_POST["userTime"])
	&& isset($_POST["questionId0"])
	&& isset($_POST["questionId1"])
	&& isset($_POST["questionId2"])
	&& isset($_POST["questionId3"])
	) {

	require_once './dbConfig/dbObject.php';
	require_once './dbConfig/astropulseDb.php';

	$timestamp = date_timestamp_get(date_create());
	$userAnswerId = crypt(urlencode($_POST["userId"] . $timestamp), rand(11, 1000));


	insertUserAnswersInDb(
		$_POST["userId"],
		$userAnswerId, 
		$_POST["hertzValue"], 
		$_POST["userTime"], 
		$_POST["questionId0"], 
		$_POST["questionId1"], 
		$_POST["questionId2"], 
		$_POST["questionId3"]
	);

} else {

	exit();

}

function insertUserAnswersInDb($userId, $userAnswerId, $hertzValue, $userTime, $answerQuest1score, $answerQuest2score, $answerQuest3score, $answerQuest4score) {

	AstropulseDB::connect();

	// Insert datas in db
	AstropulseDB::query(
		'INSERT INTO user_answer(userAnswerId, hertzValue, userTime, answerQuest1score, answerQuest2score, answerQuest3score, answerQuest4score, userFK) VALUES(:userAnswerId, :hertzValue, :userTime, :answerQuest1score, :answerQuest2score, :answerQuest3score, :answerQuest4score, :userFK)',
		// TODO -> Add type of variable like :
		// "userId"        => [$userId,  'string'],
		array(
		    "userAnswerId"      => $userAnswerId,
		    "hertzValue"        => $hertzValue,
		    "userTime"          => $userTime,
		    "answerQuest1score" => $answerQuest1score,
		    "answerQuest2score" => $answerQuest2score,
		    "answerQuest3score" => $answerQuest3score,
		    "answerQuest4score" => $answerQuest4score,
		    "userFK"            => $userId
	    )
	);

	AstropulseDB::disconnect();

}

?>