<?php  

$headers = apache_request_headers();

if ( $headers['Referer'] == 'http://localhost:8082/htw18-19_uxdesign-formGITLAB/form.php'  // TODO: Change Referer URL
	&& isset($_POST["userId"])
	&& isset($_POST["likeIt"])
	&& isset($_POST["userMail"])
	&& isset($_POST["userMessage"])
	) {

	require_once './dbConfig/dbObject.php';
	require_once './dbConfig/astropulseDb.php';

	$timestamp = date_timestamp_get(date_create());
	$userFeedbackId = crypt(urlencode($_POST["userId"] . $timestamp), rand(11, 1000));

	insertUserFeedback($_POST["userId"], $userFeedbackId, $_POST["userMessage"], $_POST["userMail"], $_POST["likeIt"]);

} else {

	exit();

}

function insertUserFeedback($userId, $userFeedbackId, $userMessage, $userMail, $likeIt) {

	AstropulseDB::connect();

	// Insert datas in db
	AstropulseDB::query(
		'INSERT INTO user_feedback(userFeedbackId, userMessage, userMail, likeIt, userFK) VALUES(:userFeedbackId, :userMessage, :userMail, :likeIt, :userFK)',
		// TODO -> Add type of variable like :
		// "userId"        => [$userId,  'string'],
		array(
		    "userFeedbackId" => $userFeedbackId,
		    "userMessage"    => $userMessage,
		    "userMail"       => $userMail,
		    "likeIt"         => $likeIt,
			"userFK"         => $userId,
	    )
	);

	AstropulseDB::disconnect();

}

?>