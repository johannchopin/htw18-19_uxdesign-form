<?php  

$headers = apache_request_headers();

if ( $headers['Referer'] == 'http://localhost:8082/htw18-19_uxdesign-formGITLAB/form.php'  // TODO: Change Referer URL
	&& isset($_POST["userId"])
	&& isset($_POST["userFavoritePulsar"])
	) {

	require_once './dbConfig/dbObject.php';
	require_once './dbConfig/astropulseDb.php';


	insertUserFavoritePulsar($_POST["userId"], $_POST["userFavoritePulsar"]);

} else {

	exit();

}

function insertUserFavoritePulsar($userId, $userFavoritePulsar) {

	AstropulseDB::connect();

	// Insert datas in db
	AstropulseDB::query(
		'UPDATE user SET userFavoritePulsar=:userFavoritePulsar WHERE userId=:userId',
		// TODO -> Add type of variable like :
		// "userId"        => [$userId,  'string'],
		array(
			"userId" 			 => $userId,
		    "userFavoritePulsar" => $userFavoritePulsar
	    )
	);

	AstropulseDB::disconnect();

}

?>