<?php

function insertUserInDbAndGetUserDatas($userDatas) {
	if (!isset($_POST["lang"])) {
		die("ERROR -> lang not defined");
	} else {
		$userDatas["lang"] = htmlspecialchars($_POST["lang"]);
		insertUserInDb( $userDatas["userId"],
						$userDatas["timestamp"],
						$userDatas["lang"],
						$userDatas["device"]
		);
		return array(
			"userId"   => $userDatas["userId"],
			"lang" => $userDatas["lang"],
		);
	}
}


function insertUserInDb($userId,$timestamp,$lang,$device) {

	AstropulseDB::connect();
	AstropulseDB::query(
		'INSERT INTO user(userId, connTimestamp, lang, device) VALUES(:userId, :connTimestamp, :lang, :device)',
		// TODO -> Add type of variable like :
		// "userId"        => [$userId,  'string'],
		array(
		    "userId"        => $userId,
		    "connTimestamp" => $timestamp,
		    "lang"          => $lang,
		    "device"        => $device,
	    )
	);
	AstropulseDB::disconnect();

}

?>
