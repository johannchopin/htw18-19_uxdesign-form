# Astropulse-Experience

#### > See demo online [here](https://astropulse.johannchopin.fr/)

----

Online Form for the UX Design cours at the HTW[18-19]


### Some rules :
  - CSS :
    - css files have to be parse by [autoprefixer](https://autoprefixer.github.io/) to add vendor prefixe
    - css files have to be minify for the production -> [cssminifier](https://cssminifier.com/)
    - Validate css in [W3C](http://jigsaw.w3.org/css-validator/)
  
  - Production version doesn´t have to contain TODO´s commentar
  - [Check](https://developers.google.com/speed/pagespeed/insights/) project speed
  - [Obfuscate](https://obfuscator.io/) JS code ?



### Big thanks to [picsvg](https://picsvg.com/) :heart: