-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 22 Janvier 2019 à 14:24
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `astropulse`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `userId` varchar(535) NOT NULL,
  `connTimestamp` varchar(535) NOT NULL,
  `localisation` varchar(535) DEFAULT NULL,
  `lang` varchar(535) NOT NULL,
  `device` varchar(535) NOT NULL,
  `userFavoritePulsar` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_answer`
--

CREATE TABLE `user_answer` (
  `userAnswerId` varchar(535) NOT NULL,
  `hertzValue` float NOT NULL,
  `userTime` float NOT NULL,
  `answerQuest1score` tinyint(4) NOT NULL,
  `answerQuest2score` tinyint(4) NOT NULL,
  `answerQuest3score` tinyint(4) NOT NULL,
  `answerQuest4score` tinyint(4) NOT NULL,
  `userFK` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_feedback`
--

CREATE TABLE `user_feedback` (
  `userFeedbackId` varchar(535) NOT NULL,
  `userMessage` text,
  `userMail` varchar(200) NOT NULL,
  `likeIt` varchar(20) DEFAULT NULL,
  `userFK` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- Index pour la table `user_answer`
--
ALTER TABLE `user_answer`
  ADD PRIMARY KEY (`userAnswerId`);

--
-- Index pour la table `user_feedback`
--
ALTER TABLE `user_feedback`
  ADD PRIMARY KEY (`userFeedbackId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
