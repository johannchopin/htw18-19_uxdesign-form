<?php

	// TODO: Change Referer URL
	// Redirect user to mainpage if he try to manualy open this file
$headers = apache_request_headers();
if (!isset($_POST['submit']) || $headers['Referer'] != 'http://localhost:8082/htw18-19_uxdesign-formGITLAB/' || !isset($_POST["lang"])) {
	header("Location: ./");
	exit();
}


require_once './phpTools/dbConfig/dbObject.php';
require_once './phpTools/dbConfig/astropulseDb.php';
require_once './phpTools/insertUser.php';
require_once './phpTools/getUserInfos.php';

$userDatas = insertUserInDbAndGetUserDatas(getUserInfos());
define('USER_ID', $userDatas["userId"]);
define('LANG', $userDatas["lang"]);
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<!-- TODO: Change form.php title -->
	<title>Astropulse</title>

	<link rel="stylesheet" type="text/css" href="./library/Swipe/swipe.min.css">
	<link rel="stylesheet" type="text/css" href="./library/Astronaut/astronaut.css">
	<link rel="stylesheet" type="text/css" href="./style/style.css">
	<link rel="stylesheet" type="text/css" href="./library/PulsarForm/pulsarForm.css">
	<link rel="icon" type="image/png" href="./img/favicon.png">
</head>
<body lang="<?php echo (htmlspecialchars($_POST['lang'])); ?>">

	<div id="warningRotate">
	    <!-- Free icon by https://icones8.fr/icons -->
		<img src="./img/rotate.png" alt="phone icon">
		<div id="starsBackground">
			<div id='stars'></div>
			<div id='stars2'></div>
			<div id='stars3'></div>
		</div>
	</div>

	<div id="loader">	
		<div class="loader-anim">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
		</div>
		<div id="starsBackground">
			<div id='stars'></div>
			<div id='stars2'></div>
			<div id='stars3'></div>
		</div>
	</div>



	<main class="swiper-container">
		<!-- Backgound by Saransh Sinha -> https://codepen.io/saransh/pen/BKJun -->
		<div id="starsBackground">
			<div id='stars'></div>
			<div id='stars2'></div>
			<div id='stars3'></div>
		</div>

		<!-- PAGE BACKGROUND -->
		<div id="bg">
			<img id="planet1" src="./img/planet1.svg" alt="">
			<img id="planet2" src="./img/planet2.svg" alt="">
			<img id="planet3" src="./img/planet3.svg" alt="">
			<img id="comet1" src="./img/comet.svg" alt="">
			<img id="shuttle1" src="./img/shuttle1.svg" alt="">
			<img id="shuttle2" src="./img/shuttle2.svg" alt="">
			<img id="myLogo" src="./img/myLogo.svg" alt="">
		</div>
		

		<div class="swiper-wrapper swiper-no-swiping">
			
			<div class="swiper-slide">
				<div id="astronautCtn1" class="astronaut-ctn">			
					<div id="astronaut">
						<div id="headsetBlur"></div>
						<img src="./img/astronaut.svg" alt="Astropulse character">
					</div>
				</div>
				<div class="goNextQuestionButton"></div>
			</div>

			<div class="swiper-slide">
				<div id="pulsar1"></div>
				<div class="goNextQuestionButton"></div>
			</div>		

			<div class="swiper-slide">
				<div id="pulsar2"></div>
				<div class="goNextQuestionButton"></div>
			</div>
			
			<div class="swiper-slide">
				<div id="pulsar3"></div>
				<div class="goNextQuestionButton"></div>
			</div>
			
			<div class="swiper-slide">
				<h1 id="WDYPQuestion" class="questions"></h1>
				<div id="pulsesAnimationCtn">
					<label class="pulse-animation-choice">
						<div class="pulse-animation-ctn">
							<div class="pulse-animation"></div>
						</div>
						<input type="radio" name="favoritePulsar" id="pulsarChoice1" value="0.5">
						<label for="pulsarChoice1" class="radio"></label>
					</label>
					<label class="pulse-animation-choice">
						<div class="pulse-animation-ctn">
							<div class="pulse-animation"></div>
						</div>
						<input type="radio" name="favoritePulsar" id="pulsarChoice2" value="1">
						<label for="pulsarChoice2" class="radio"></label>
					</label>
					<label class="pulse-animation-choice">
						<div class="pulse-animation-ctn">
							<div class="pulse-animation"></div>
						</div>
						<input type="radio" name="favoritePulsar" id="pulsarChoice3" value="2">
						<label for="pulsarChoice3" class="radio"></label>
					</label>
				</div>
				<div id="userTimesCtn">
					<p></p>
					<p></p>
					<p></p>
				</div>
				<div id="showUserTimesButton"></div>
				<div class="goNextQuestionButton"></div>
			</div>

			<div class="swiper-slide">
				<div id="astronautCtn2" class="astronaut-ctn">			
					<div id="astronaut">
						<div id="headsetBlur"></div>
						<img src="./img/astronaut.svg" alt="Astropulse character">
					</div>
				</div>

				<form action="" id="feedbackForm">
					<div class="row">
						<p id="likeItQuestion"></p>

						<div class="entry">
							<input type="radio" name="likeIt" id="dontLikeIt" value="dontLikeIt">
							<label for="dontLikeIt" class="feedback-form-radio">&#x1f44e;</label>

							<input type="radio" name="likeIt" id="likeIt" value="likeIt" checked>
							<label for="likeIt" class="feedback-form-radio">&#x1f44d;</label>
						</div>
					</div>
					<div class="row">
						<input type="email" id="email" class="row_input" required>
						<label for="email" class="label"></label>
					</div>
					<div class="row">
						<textarea name="message" id="message" class="row_input" cols="30" rows="5" required></textarea>
						<label for="message" class="label"></label>
					</div>
					<div class="row">
						<div id="feedbackFormSubmit" class="button"></div>
					</div>
					<p id="escapeForm"></p>
			    </form>
			</div>

			<div class="swiper-slide">
				
			</div>
		</div>
	</main>


<script src="./library/Swipe/swipe.js"></script>
<script src="./library/jquery.js"></script>
<script src="./library/Astronaut/astronaut.js"></script>
<script>
	// Some super global variables
	const USER_ID = '<?php echo (htmlspecialchars(USER_ID)); ?>';
	const LANG = '<?php echo (htmlspecialchars(LANG)); ?>';
	const USER_TIMES = {};
</script>
<script src="./library/PulsarForm/pulsarForm.js"></script>
<script src="./script/allSentencesDb.js"></script>
<script src="./script/script.js"></script>
</body>
</html>